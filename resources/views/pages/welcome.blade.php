@extends('templates.main')

@section('title', '| SISTEMA PARA PRESTAÇÃO DE DÍZIMOS')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="jumbotron">
          <!-- <h1>Bem-vindo(a) ao SMCA</h1> -->
          <p class="lead">SPC - SISTEMA DE PRESTAÇÃO DE CONTAS</p>
          <!-- <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p> -->
        </div>
      </div>
    </div>


    <div class="row">
        <div class="col-md-8">
            <div class="row">
        		<div class="col-md-12">
                    <div class="col-md-9">
            			<h3>Últimos de Lançamentos</h3>
                    </div>

                    <div class="col-md-3">
                        @if (Auth::check())
                			<a href="{{ route('lancamentos.create') }}" class="btn btn-block btn-primary btn-h1-spacing">Adicionar Lançamento</a>
                        @endif
            		</div>
        		</div>

        		<div class="col-md-12">
        			<hr>
        		</div>
        	</div>
            <table class="table table-hover table-striped">
				<thead>
					<th>#</th>
					<th>Tipo</th>
					<th>Valor</th>
					<th>Data Referência</th>
					<th>Data Lançamento</th>
					<th>Funcionário</th>
					<th>Descrição</th>

					{{-- <th></th> --}}
				</thead>

				<tbody>

					@foreach ($lancamentos as $l)

						<tr>
							<th>{{ $l->id }}</th>
							<td>{{ $l->tipo->descricao }}</td>
							<td>{{ $l->valor }}</td>
							<td>{{ $l->data_ref }}</td>
							<td>{{ $l->created_at }}</td>
							<td>{{ $l->voluntario->name }}</td>
							<td>{{ $l->descricao }}</td>
							{{-- <td><a href="{{ route('lancamentos.show', $l->id) }}" class="btn btn-default btn-sm">Visualizar</a></td> --}}
						</tr>

					@endforeach

				</tbody>
			</table>
        </div>

        <div class="col-md-3 col-md-offset-1">
            <div id="saldo"
                @if($saldo > 0)
                    class="bg-green"
                @endif

                @if($saldo == 0)
                    class="bg-gray"
                @endif

                @if($saldo < 0)
                    class="bg-red"
                @endif
            >
                <label for="valor">Saldo </label>
                <div id="valor">
                    R$ <span class="saldo-numero">{{ $saldo }}</span>
                </div>
            </div>
        </div>
        {{-- <div class="col-md-8">
        @foreach($lancamentos as $l)
          <div class="form-group" style="float: left; margin-right: 10px;">
            <img style="border-radius: 50%;" src="{{ asset('images/' . $l->voluntario->photo_path) }}" height="100">
          </div>
          <div class="post">
            <h3>{{ $l->voluntario->name }}</h3>
            <p>
              {{ $l->tipo->descricao }} em {{ $l->data }}
            </p>
          </div>
          <div style='clear: both;'></div>
          <hr>
        @endforeach
        </div> --}}

      {{-- <div class="col-md-3 col-md-offset-1">
        <div>
          <h3>Próximos da lista</h3>
        </div>
        <div>
          @foreach($queue as $q)
            <div class="form-group" style="float: left; margin-right: 10px;">
              <img style="border-radius: 50%;" src="{{ asset('images/' . $q['photo']) }}" height="50">
            </div>
            <div class="post">
              <h3>{{ $q["nome"] }} ({{ $q["contagem"] }})</h3>
              <p>
                {{ $q["ultima_data"] }}
              </p>
            </div>
            <hr>
          @endforeach
        </div>
      </div> --}}
    </div>

  </div>
@endsection
