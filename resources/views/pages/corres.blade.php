@extends('templates.main')

@section('title', '| SPD')

@section('content')
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="jumbotron">
          <!-- <h1>Bem-vindo(a) ao SMCA</h1> -->
          <p class="lead">SPD - SISTEMA PARA PRESTAÇÃO DE DÍZIMOS</p>
          <!-- <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a></p> -->
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-md-8">
        @foreach($lancamentos as $l)
          <div class="form-group" style="float: left; margin-right: 10px;">
            <img style="border-radius: 50%;" src="{{ asset('images/' . $l->voluntario->photo_path) }}" height="100">
          </div>
          <div class="post">
            <h3>{{ $l->voluntario->name }}</h3>
            <p>
              {{ $l->acao->tipo }} em {{ $l->data }}
            </p>
          </div>
          <div style='clear: both;'></div>
          <hr>
        @endforeach
      </div>

      <div class="col-md-3 col-md-offset-1">
        <div>
          <h3>Próximos da lista</h3>
        </div>
        <div>
          @foreach($queue as $q)
            <div class="form-group" style="float: left; margin-right: 10px;">
              <img style="border-radius: 50%;" src="{{ asset('images/' . $q['photo']) }}" height="50">
            </div>
            <div class="post">
              <h3>{{ $q["nome"] }} ({{ $q["contagem"] }})</h3>
              <p>
                {{ $q["ultima_data"] }}
              </p>
            </div>
            <hr>
          @endforeach
        </div>
      </div>
    </div>

  </div>
@endsection
