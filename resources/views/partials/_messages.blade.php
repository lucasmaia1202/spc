@if (Session::has('success'))

	<div class="alert alert-success" role="alert">
		<strong>Sucesso:</strong> {{ Session::get('success') }}
	</div>

@endif

@if (count($errors) > 0)

	<div class="alert alert-danger" role="alert">
		<strong>Erros:</strong> 
		<ul>
			@foreach ($errors->all() as $e)
				<li>{{ $e }}</li>
			@endforeach			
		</ul>
	</div>

@endif