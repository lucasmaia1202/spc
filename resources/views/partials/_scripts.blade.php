<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<!-- DataTables -->
{{ Html::script('datatables.net/js/jquery.dataTables.min.js')}}
{{ Html::script('datatables.net-bs/js/dataTables.bootstrap.min.js')}}

{{ Html::script('js/bootstrap-table.js') }}
{{ Html::script('js/bootstrap-table-filter-control.js') }}
{{ Html::script('inputmask/dist/jquery.mask.min.js') }}

@yield('js')
