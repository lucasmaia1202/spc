<div>
	<h3>Novo Lançamento no SPC - Sistema de Prestação de Contas.</h3>

	<hr>

	<div>
		<span>Lançamento #</span>{{ $lancamento->id }}
		<div>
			{{ $lancamento->tipo->descricao }} lançada em {{ $lancamento->data }} em nome do(a) funcionário(a)
			{{ $lancamento->voluntario->name }} com o valor de {{ $lancamento->valor }}.
		</div>
	</div>

	<hr>

	<br>

	{{-- <div>
		<div class="col-md-3 col-md-offset-1">
			<div>
				<h3>Relatório</h3>
			</div>
			<div>
				@foreach($queue as $q)
					<div class="post">
						<h3>{{ $q["nome"] }} ({{ $q["contagem"] }})</h3>
						<p>
							{{ $q["ultima_data"] }}
						</p>
					</div>
					<hr>
				@endforeach
			</div>
		</div>
	</div> --}}
</div>
