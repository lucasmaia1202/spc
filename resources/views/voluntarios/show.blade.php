@extends('templates.main')

@section('title', '| Voluntário')

@section('content')

	<div class="row">
		<h1 class="text-center">Volutário</h1>	
	</div>
	

	<div class="row">
		<div class="col-md-8">
			<div class="form-group" style="float: left; margin-right: 10px;">
				<img src="{{asset('images/' . $voluntario->photo_path)}}" height="200">
			</div>
			<div>
				<div class="form-group">
					<label class="lead">Nome: </label> {{ $voluntario->name }}
				</div>
				<div class="form-group">
					<label class="lead">Email: </label> {{ $voluntario->email }}
				</div>
				<div class="form-group">
					<label class="lead">Data de Nascimento: </label> {{ $voluntario->birthday }}
					<label class="lead">Turno: </label> {{ $voluntario->shift }}
				</div>				
			</div>
			<div style='clear: both;'></div>
		</div>

		<div class="col-md-4">
			<div class="well">
				<dl class="dl-horizontal">
					<dt>Criado em:</dt>
					<dd>
						{{ date('d/m/Y', strtotime($voluntario->created_at)) }} às 
						{{ date('H:s', strtotime($voluntario->created_at)) }}
					</dd>
				</dl>

				<dl class="dl-horizontal">
					<dt>Atualizado em:</dt>
					<dd>
						{{ date('d/m/Y', strtotime($voluntario->updated_at)) }} às
						{{ date('H:s', strtotime($voluntario->updated_at)) }}
					</dd>
				</dl>
				<hr>

				<div class="row">
					<div class="col-sm-6">
						{!! Html::linkRoute('voluntarios.edit', 'Editar', [$voluntario->id], 
							['class' => 'btn btn-primary btn-block']) !!}
					</div>
					<div class="col-sm-6">
						{!! Form::open(['method' => 'DELETE', 'route' => ['voluntarios.destroy', $voluntario->id]]) !!}

							{!! Form::submit('Deletar', ['class' => 'btn btn-danger btn-block']) !!}

						{!! Form::close() !!}
					</div>
				</div>

			</div>
		</div>
	</div>

@endsection