@extends('templates.main')

@section('title', '| Voluntários')

@section('content')

	<div class="row">
		<div class="col-md-10">
			<h1>Lista de Voluntários</h1>
		</div>

		<div class="col-md-2">
			<a href="{{ route('voluntarios.create') }}" class="btn btn-block btn-primary btn-h1-spacing">Adicionar Voluntário</a>
		</div>
		<div class="col-md-12">
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<table class="table">
				<thead>
					<th>#</th>
					<th>Nome</th>
					<th>Email</th>
					<th>Data de Nascimento</th>
					<th>Turno</th>
					<th></th>
				</thead>

				<tbody>
					
					@foreach ($voluntarios as $v)

						<tr>
							<th>{{ $v->id }}</th>
							<td>{{ $v->name }}</td>
							<td>{{ $v->email }}</td>
							<td>{{ $v->birthday }}</td>
							<td>{{ $v->shift }}</td>
							<td><a href="{{ route('voluntarios.show', $v->id) }}" class="btn btn-default btn-sm">Visualizar</a></td>
						</tr>

					@endforeach

				</tbody>
			</table>
		</div>
	</div>

@endsection