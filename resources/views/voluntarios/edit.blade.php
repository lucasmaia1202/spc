@extends('templates.main')

@section('title', '| Editar Voluntário')

@section('content')

	<div class="row">
		<h1 class="text-center">Volutário</h1>	
	</div>
	

	<div class="row">
		{!! Form::model($voluntario, ['method' => 'PUT', 'route' => ['voluntarios.update', $voluntario->id], 'files' => true]) !!}
		<div class="col-md-8">
			<div class="form-group">
				{{ Form::label('name', 'Nome:') }}
				{{ Form::text('name', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '255']) }}
			</div>

			<div class="form-group">
				{{ Form::label('email', 'Email:') }}
				{{ Form::text('email', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '255', 'data-parsley-type' => 'email']) }}
			</div>

			<div class="row form-group">
				<div class="form-group col-md-6">
					{{ Form::label('birthday', 'Data de Nascimento:') }}
					{{ Form::text('birthday', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '10', 'data-parsley-type' => 'date']) }}							
				</div>
				<div class="form-group col-md-6">
					{{ Form::label('shift', 'Turno:') }}
					{{ Form::select('shift', ['M' => 'Matutino', 'V' => 'Vespertino', 'I' => 'Integral'], null, ['class' => 'form-control']) }}								
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('image', 'Imagem de Perfil:') }}
				{{ Form::file('image') }}
			</div>
		</div>

		<div class="col-md-4">
			<div class="well">
				<dl class="dl-horizontal">
					<dt>Criado em:</dt>
					<dd>
						{{ date('d/m/Y', strtotime($voluntario->created_at)) }} às 
						{{ date('H:s', strtotime($voluntario->created_at)) }}
					</dd>
				</dl>

				<dl class="dl-horizontal">
					<dt>Atualizado em:</dt>
					<dd>
						{{ date('d/m/Y', strtotime($voluntario->updated_at)) }} às
						{{ date('H:s', strtotime($voluntario->updated_at)) }}
					</dd>
				</dl>
				<hr>

				<div class="row">
					<div class="col-sm-6">
						{!! Html::linkRoute('voluntarios.show', 'Cancelar', [$voluntario->id], 
							['class' => 'btn btn-danger btn-block']) !!}
					</div>
					<div class="col-sm-6">
						{!! Form::submit('Salvar', ['class' => 'btn btn-success btn-block']) !!}
					</div>
				</div>

			</div>
		</div>
		{!! Form::close() !!}
	</div>

@endsection