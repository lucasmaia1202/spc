@extends('templates.main')

@section('title', '| Novo Voluntário')

@section('css')
	{!! Html::style('css/parsley.css') !!}
	{!! Html::style('css/bootstrap-datetimepicker.min.css') !!}
@endsection

@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">

			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<h1 class="form-title">Criar Novo Voluntário</h1>
				</div>
			</div>

			<hr>

			<div class="row">
				{!! Form::open(['route' => 'voluntarios.store', 'data-parsley-validate' => '', 'files' => true]) !!}

					<div class="form-group">
						{{ Form::label('name', 'Nome:') }}
						{{ Form::text('name', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '255']) }}
					</div>

					<div class="form-group">
						{{ Form::label('email', 'Email:') }}
						{{ Form::text('email', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '255', 'data-parsley-type' => 'email']) }}
					</div>

					<div class="row form-group">
						<div class="form-group col-md-6">
							{{ Form::label('birthday', 'Data de Nascimento:') }}
							{{ Form::text('birthday', null, ['class' => 'form-control', 'required' => '', 'maxlength' => '10', 'data-parsley-type' => 'date']) }}							
						</div>
						<div class="form-group col-md-6">
							{{ Form::label('shift', 'Turno:') }}
							{{ Form::select('shift', ['M' => 'Matutino', 'V' => 'Vespertino', 'I' => 'Integral'], null, ['class' => 'form-control']) }}								
						</div>
					</div>

					<div class="form-group">
						{{ Form::label('image', 'Imagem de Perfil:') }}
						{{ Form::file('image') }}
					</div>

					<div class="form-group">
						{{ Form::submit('Criar Voluntário', ['class' => 'btn btn-success btn-lg btn-block']) }}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>

@endsection

@section('js')
	{!! Html::script('js/parsley/parsley.min.js') !!}
	{!! Html::script('js/moment-with-locales.min.js') !!}
	{!! Html::script('js/transition.js') !!}
	{!! Html::script('js/collapse.js') !!}
	{!! Html::script('js/bootstrap-datetimepicker.min.js') !!}
<!-- 
	<script type="text/javascript">
        $(function () {
            $('#birthday').datetimepicker({
				locale: 'pt-Br',
				format: 'DD/MM/YYYY'
			});
		});
    </script> -->
@endsection