@extends('templates.main')

@section('title', '| Lançamentos')

@section('content')

	<div class="row">
		<div class="col-md-10">
			<h1>Lista de Lançamentos</h1>
		</div>

		<div class="col-md-2">
			<a href="{{ route('lancamentos.create') }}" class="btn btn-block btn-primary btn-h1-spacing">Adicionar Lançamento</a>
		</div>
		<div class="col-md-12">
			<hr>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<table id="lancamentos" class="table table-hover table-striped">
				<thead>
					<th>#</th>
					<th>Tipo</th>
					<th>Valor</th>
					<th>Data Referência</th>
					<th>Data Lançamento</th>
					<th>Voluntáio</th>
					<th>Descrição</th>
					<th></th>
				</thead>

				<tbody>

					@foreach ($lancamentos as $l)

						<tr>
							<th>{{ $l->id }}</th>
							<td>{{ $l->tipo->descricao }}</td>
							<td>{{ $l->valor }}</td>
							<td>{{ $l->data_ref }}</td>
							<td>{{ $l->created_at }}</td>
							<td>{{ $l->voluntario->name }}</td>
							<td>{{ $l->descricao }}</td>
							<td><a href="{{ route('lancamentos.show', $l->id) }}" class="btn btn-default btn-sm">Visualizar</a></td>
						</tr>

					@endforeach

				</tbody>
			</table>
		</div>
	</div>

@endsection

@section('js')
	<script>
        initDatatable('lancamentos');
    </script>
@endsection
