@extends('templates.main')

@section('title', '| Novo Laçamento')

@section('css')
	{!! Html::style('css/parsley.css') !!}
@endsection

@section('content')

	<div class="row">
		<div class="col-md-8 col-md-offset-2">

			<div class="row">
				<div class="col-md-8 col-md-offset-3">
					<h1 class="form-title">Criar Novo Laçamento</h1>
				</div>
			</div>

			<hr>

			<div class="row">
				{!! Form::open(['route' => 'lancamentos.store', 'data-parsley-validate' => '']) !!}

					<div class="row form-group">
						<div class="form-group col-md-4">
							{{ Form::label('valor', 'Valor:') }}
							{{ Form::text('valor', null, ['class' => 'form-control money', 'required' => '']) }}
						</div>
						<div class="form-group col-md-4">
							{{ Form::label('tipo_id', 'Tipo:') }}
							{{ Form::select('tipo_id', $tipos, null, ['class' => 'form-control']) }}
						</div>
						<div class="form-group col-md-4">
							{{ Form::label('data_ref', 'Data Referência:') }}
							{{ Form::text('data_ref', $hoje, ['class' => 'form-control date', 'required' => '', 'maxlength' => '10']) }}
						</div>
					</div>

					<div class="form-group">
						{{ Form::label('voluntario_id', 'Funcionário:') }}
						{{ Form::select('voluntario_id', $voluntarios, null, ['class' => 'form-control']) }}
					</div>

					<div class="form-group">
						{{ Form::label('descricao', 'Descrição:') }}
						{{ Form::textarea('descricao', null, ['class' => 'form-control', 'required' => '']) }}
					</div>

					<div class="form-group">
						{{ Form::submit('Criar Laçamento', ['class' => 'btn btn-success btn-lg btn-block']) }}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>

@endsection

@section('js')
	{!! Html::script('js/parsley/parsley.min.js') !!}

    <script>
        $(document).ready(function(){
  			$('.money').mask('#.##0,00', {reverse: true});
			$('.date').mask("00r0000", {
				translation: {
				  'r': {
				    pattern: /[\/]/,
				    fallback: '/'
				  },
				  placeholder: "__/____"
				}
			});
        });
    </script>
@endsection
