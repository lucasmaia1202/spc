@extends('templates.main')

@section('title', '| Editar Lançamento')

@section('content')

	<div class="row">
		<h1 class="text-center">Lançamento</h1>
	</div>


	<div class="row">
		{!! Form::model($lancamento, ['method' => 'PUT', 'route' => ['lancamentos.update', $lancamento->id]]) !!}
		<div class="col-md-8">

			<div class="row form-group">
				<div class="form-group col-md-4">
					{{ Form::label('valor', 'Valor:') }}
					{{ Form::text('valor', null, ['class' => 'form-control money', 'required' => '']) }}
				</div>
				<div class="form-group col-md-6">
					{{ Form::label('data_ref', 'Data Referência:') }}
					{{ Form::text('data_ref', null, ['class' => 'form-control date', 'required' => '', 'maxlength' => '10']) }}
				</div>
				<div class="form-group col-md-6">
					{{ Form::label('tipo_id', 'Tipo:') }}
					{{ Form::select('tipo_id', $tipos, null, ['class' => 'form-control']) }}
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('voluntario_id', 'Voluntário:') }}
				{{ Form::select('voluntario_id', $voluntarios, null, ['class' => 'form-control']) }}
			</div>

			<div class="form-group">
				{{ Form::label('descricao', 'Descrição:') }}
				{{ Form::textarea('descricao', null, ['class' => 'form-control', 'required' => '']) }}
			</div>

		</div>

		<div class="col-md-4">
			<div class="well">
				<dl class="dl-horizontal">
					<dt>Criado em:</dt>
					<dd>
						{{ date('d/m/Y', strtotime($lancamento->created_at)) }} às
						{{ date('H:s', strtotime($lancamento->created_at)) }}
					</dd>
				</dl>

				<dl class="dl-horizontal">
					<dt>Atualizado em:</dt>
					<dd>
						{{ date('d/m/Y', strtotime($lancamento->updated_at)) }} às
						{{ date('H:s', strtotime($lancamento->updated_at)) }}
					</dd>
				</dl>
				<hr>

				<div class="row">
					<div class="col-sm-6">
						{!! Html::linkRoute('lancamentos.show', 'Cancelar', [$lancamento->id],
							['class' => 'btn btn-danger btn-block']) !!}
					</div>
					<div class="col-sm-6">
						{!! Form::submit('Salvar', ['class' => 'btn btn-success btn-block']) !!}
					</div>
				</div>

			</div>
		</div>
		{!! Form::close() !!}
	</div>

@endsection



@section('js')
	{!! Html::script('js/parsley/parsley.min.js') !!}

    <script>
        $(document).ready(function(){
  			$('.money').mask('#.##0,00', {reverse: true});
			$('.date').mask("00r0000", {
				translation: {
				  'r': {
				    pattern: /[\/]/,
				    fallback: '/'
				  },
				  placeholder: "__/____"
				}
			});
        });
    </script>
@endsection
