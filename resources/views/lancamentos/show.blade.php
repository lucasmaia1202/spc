@extends('templates.main')

@section('title', '| Lançamento')

@section('content')

	<div class="row">
		<h1 class="text-center">Lançamento</h1>
	</div>


	<div class="row">
		<div class="col-md-8">
			<div class="form-group">
				<label class="lead">Valor: </label> {{ $lancamento->valor }}
			</div>
			<div class="form-group">
				<label class="lead">Funcionário(a): </label> {{ $lancamento->voluntario->name }}
			</div>
			<div class="form-group">
				<label class="lead">Data Referência: </label> {{ $lancamento->data_ref }}
				<label class="lead">Tipo: </label> {{ $lancamento->tipo->descricao }}
			</div>
			<div class="form-group">
				<label class="lead">Descrição: </label> {{ $lancamento->descricao }}
			</div>
		</div>

		<div class="col-md-4">
			<div class="well">
				<dl class="dl-horizontal">
					<dt>Criado em:</dt>
					<dd>
						{{ date('d/m/Y', strtotime($lancamento->created_at)) }} às
						{{ date('H:s', strtotime($lancamento->created_at)) }}
					</dd>
				</dl>

				<dl class="dl-horizontal">
					<dt>Atualizado em:</dt>
					<dd>
						{{ date('d/m/Y', strtotime($lancamento->updated_at)) }} às
						{{ date('H:s', strtotime($lancamento->updated_at)) }}
					</dd>
				</dl>
				<hr>

				<div class="row">
					<div class="col-sm-6">
						{!! Html::linkRoute('lancamentos.edit', 'Editar', [$lancamento->id],
							['class' => 'btn btn-primary btn-block']) !!}
					</div>
					<div class="col-sm-6">
						{!! Form::open(['method' => 'DELETE', 'route' => ['lancamentos.destroy', $lancamento->id]]) !!}

							{!! Form::submit('Deletar', ['class' => 'btn btn-danger btn-block']) !!}

						{!! Form::close() !!}
					</div>
				</div>

			</div>
		</div>
	</div>

@endsection
