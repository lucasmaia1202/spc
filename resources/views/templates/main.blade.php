<!DOCTYPE html>
<html lang="en">

  <head>

    @include('partials._head')

  </head>

  <body>

    @include('partials._navbar')

    <div class="container">


      <!-- {!! Auth::check() ? '<div class="alert alert-success" role="alert">Logged In</div>' : '<div class="alert alert-danger" role="alert">Logged Out</div>' !!} -->

      @include('partials._messages')

      @yield('content')

      @include('partials._footer')

    </div>

    <script type="text/javascript">

        function initDatatable(tableId) {
            const dataTable = $('#' + tableId).DataTable({
                'retrieve': true,
                'paging': true,
                'lengthChange': true,
                'searching': true,
                'ordering': true,
                'info': true,
                'autoWidth': true,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ linhas por página",
                    "zeroRecords": "Nada encontrado",
                    "info": "Mostrando página _PAGE_ de _PAGES_ ( _MAX_ registros )",
                    "infoEmpty": "Sem dados a mostrar",
                    "infoFiltered": "(filtrado _MAX_)",
                    "search": "Procurar ",
                    "paginate": {
                        "previous": "Anterior",
                        "next": "Próximo"
                    }
                }
            });

        }

    </script>

    @include('partials._scripts')

  </body>

</html>
