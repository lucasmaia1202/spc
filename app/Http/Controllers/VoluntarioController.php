<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voluntario;
use Session;
use Image;

class VoluntarioController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // create a variable and store all the volunteers in it from the database
        $voluntarios = Voluntario::all();

        // return a view and pass in the above variable
        return view('voluntarios.index')->with('voluntarios', $voluntarios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('voluntarios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'birthday' => 'required|Date',
            'shift' => 'required'

        ]);

        // store in the database
        $voluntario = new Voluntario;
        
        $voluntario->name = $request->name;
        $voluntario->email = $request->email;
        $voluntario->birthday = $request->birthday;
        $voluntario->shift = $request->shift;

        if($request->hasfile('image')){
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($image)->save($location);

            $voluntario->photo_path = $filename;
        }
        else {
            $voluntario->photo_path = "default-profile.png";
        }

        $voluntario->save();

        Session::flash('success', 'O voluntário foi criado com sucesso!');

        // redirect to another page
        return redirect()->route('voluntarios.show', $voluntario->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $voluntario = Voluntario::find($id);

        return view('voluntarios.show')->with('voluntario', $voluntario);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // find the volunteer in the database and save as a var
        $voluntario = Voluntario::find($id);

        // return the view with the data
        return view('voluntarios.edit')->with('voluntario', $voluntario);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate the data
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'birthday' => 'required|Date',
            'shift' => 'required'

        ]);

        // store in the database
        $voluntario = Voluntario::find($id);
        
        $voluntario->name = $request->name;
        $voluntario->email = $request->email;
        $voluntario->birthday = $request->birthday;
        $voluntario->shift = $request->shift;

        if($request->hasfile('image')){
            $image = $request->file('image');
            $filename = time() . '.' . $image->getClientOriginalExtension();
            $location = public_path('images/' . $filename);
            Image::make($image)->save($location);

            $voluntario->photo_path = $filename;
        }

        $voluntario->save();

        Session::flash('success', 'O voluntário foi editado com sucesso!');

        // redirect to another page
        return redirect()->route('voluntarios.show', $voluntario->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $voluntario = Voluntario::find($id);

        $voluntario->delete();

        Session::flash('success', 'Voluntário deletado com sucesso.');
        return redirect()->route('voluntarios.index');
    }
}
