<?php

namespace App\Http\Controllers;

use App\Lancamento;
use App\Voluntario;

class PagesController extends Controller {

	public function getIndex(){
		$lancamentos = Lancamento::all();
		$show = Lancamento::orderBy('created_at', 'desc')->limit(8)->get();

		$saldo = 0.00;
		foreach ($lancamentos as $l) {
			$tipo = ($l->tipo_id == 1) ? 1.0 : -1.0;
			$saldo += ((float)$l->valor) * $tipo;
		}

		$saldo = number_format((string)$saldo, 2, ',', '');

		return view('pages.welcome')->with('lancamentos', $show)->with('saldo', $saldo);
	}

	public function getCorres(){
		$lancamentos = Lancamento::where('acao_id','=',2)->orderBy('data', 'desc')->get();
		$queue = $this->getQueue(2);

		return view('pages.corres')->with('lancamentos', $lancamentos)->with('queue', $queue);
	}

	public function getQueue($tipo){
		$volutario = Voluntario::all();


		foreach($volutario as $v){
			$count = Lancamento::where('voluntario_id', '=', $v->id)->where('acao_id', '=', $tipo)->count();
			$last = Lancamento::where('voluntario_id', '=', $v->id)->where('acao_id', '=', $tipo)
					->orderBy('data', 'desc')->get()->first();

			$queue[$v->id]["contagem"] = $count;
			if($last!=NULL){
				$queue[$v->id]["ultima_data"] = $last->data;
			}
			else {
				$queue[$v->id]["ultima_data"] = Date("2000-01-01");
			}
			$queue[$v->id]["nome"] = $v->name;
			$queue[$v->id]["photo"] = $v->photo_path;
		}

		// array_multisort($queue, SORT_ASC);

		usort($queue, function($a, $b) {
		    $retval = $a['contagem'] <=> $b['contagem'];
		    if ($retval == 0) {
		        $retval = $a['ultima_data'] <=> $b['ultima_data'];
		    }
		    return $retval;
		});

		return $queue;
	}

}
