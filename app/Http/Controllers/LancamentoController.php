<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lancamento;
use App\Voluntario;
use App\Tipo;
use Session;
use Mail;

class LancamentoController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // create a variable and store all the volunteers in it from the database
        $lancamentos = Lancamento::orderBy('data_ref', 'desc')->get();

        // return a view and pass in the above variable
        return view('lancamentos.index')->with('lancamentos', $lancamentos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $voluntarios = Voluntario::orderBy('name')->pluck('name', 'id');
        $tipos = Tipo::orderBy('descricao')->pluck('descricao', 'id');
        return view('lancamentos.create')->with('voluntarios', $voluntarios)->with('tipos', $tipos)->with('hoje', date('m/Y'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validate the data
        $this->validate($request, [
            'voluntario_id' => 'required',
            'data_ref' => 'required',
            'tipo_id' => 'required',
            'valor' =>'required'
        ]);

        // store in the database
        $lancamento = new Lancamento;

        $lancamento->valor = str_replace(",", ".", str_replace(".", "", $request->valor));
        $lancamento->voluntario_id = $request->voluntario_id;
        $lancamento->data_ref = $request->data_ref;
        $lancamento->tipo_id = $request->tipo_id;
        $lancamento->descricao = $request->descricao;

        $lancamento->save();

        $voluntarios = Voluntario::all();

        // foreach ($voluntarios as $v) {
        //     $data = array(
        //         'email' => $v->email,
        //         'lancamento' => $lancamento
        //         // 'queue' => app('App\Http\Controllers\PagesController')->getQueue($lancamento->acao_id)
        //     );
        //
        //     Mail::send('emails.lancamento', $data, function($message) use ($data){
        //         $message->from('noreplay@spc.huufma.br');
        //         $message->to($data['email']);
        //         $message->subject('Lancamento em SMCA');
        //     });
        // }

        Session::flash('success', 'O lançamento foi criado com sucesso!');

        // redirect to another page
        return redirect()->route('lancamentos.show', $lancamento->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lancamento = Lancamento::find($id);

        return view('lancamentos.show')->with('lancamento', $lancamento);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // find the volunteer in the database and save as a var
        $lancamento = Lancamento::find($id);
        $voluntarios = Voluntario::orderBy('name')->pluck('name', 'id');
        $tipos = Tipo::orderBy('descricao')->pluck('descricao', 'id');

        // return the view with the data
        return view('lancamentos.edit')
            ->with('lancamento', $lancamento)
            ->with('voluntarios', $voluntarios)
            ->with('tipos', $tipos);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validate the data
        $this->validate($request, [
            'voluntario_id' => 'required',
            'data_ref' => 'required',
            'tipo_id' => 'required',
            'valor' =>'required'

        ]);

        // store in the database
        $lancamento = Lancamento::find($id);

        $lancamento->valor = str_replace(",", ".", str_replace(".", "", $request->valor));
        $lancamento->voluntario_id = $request->voluntario_id;
        $lancamento->data_ref = $request->data_ref;
        $lancamento->tipo_id = $request->tipo_id;
        $lancamento->descricao = $request->descricao;

        $lancamento->save();

        Session::flash('success', 'O lançamento foi criado com sucesso!');

        // redirect to another page
        return redirect()->route('lancamentos.show', $lancamento->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lancamento = Lancamento::find($id);

        $lancamento->delete();

        Session::flash('success', 'Lançamento deletado com sucesso.');
        return redirect()->route('lancamentos.index');
    }
}
