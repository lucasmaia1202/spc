<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voluntario extends Model
{
    //

    public function lancamentos()
    {
    	return $this->hasMany('App\Lancamento');
    }
}
