<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lancamento extends Model
{

    protected $table = 'lancamentos';

    public function tipo()
    {
    	return $this->belongsTo('App\Tipo');
    }

    public function voluntario()
    {
    	return $this->belongsTo('App\Voluntario');
    }

}
